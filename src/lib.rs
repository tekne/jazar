/*!
A simple library for conjugation in the Arabic language.

No guarantees of any kind of correctness; mainly developed to help me learn the verb forms
*/
#![forbid(unsafe_code, missing_debug_implementations, missing_docs)]

use nom::bytes::complete::tag;
use nom::combinator::map;

/// The letters of the Arabic alphabet
///
/// These are represented as an 8-bit integer in the range 0..29 given by that letter's position in the (common) Hijā'ī order
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[repr(u8)]
pub enum ArabicLetter {
    /// Alef
    ا,
    /// Ba
    ب,
    /// Ta
    ت,
    /// Tha
    ث,
    /// Jim
    ج,
    /// Ha
    ح,
    /// Kha
    خ,
    /// Da
    د,
    /// Dha
    ذ,
    /// Ra
    ر,
    /// Za
    ز,
    /// Sin
    س,
    /// Shin
    ش,
    /// Sad
    ص,
    /// Dad
    ض,
    /// Tah
    ط,
    /// Zah
    ظ,
    /// Ayn
    ع,
    /// Ghayn
    غ,
    /// Fa
    ف,
    /// Qaf
    ق,
    /// Ka
    ك,
    /// Lam
    ل,
    /// Mim
    م,
    /// Nun
    ن,
    /// Hah
    ه,
    /// Waw
    و,
    /// Yah
    ي,
    /// Hamzah
    ء,
}

/// Arabic tashkil
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Tashkil {
    /// A fathah
    Fathah,
    /// A kasrah
    Kasrah,
    /// A dammah
    Dammah,
    /// An alif khanjariyah
    AlifKhanjariyah,
    /// A maddah
    Maddah,
    /// An alif waslah
    AlifWaslah,
    /// A sukun
    Sukun,
    /// A fathah tanwin
    FathahTanwin,
    /// A kasrah tanwin
    KasrahTanwin,
    /// A dammah tanwin
    DammahTanwin,
}

impl Tashkil {
    /// Parse a tashkil from an input string
    pub fn parse(input: &str) -> IResult<&str, Tashkil> {
        alt((
            map(tag("ً"), |_| Tashkil::FathahTanwin),
            map(tag("ٍ"), |_| Tashkil::KasrahTanwin),
            map(tag("ٌ"), |_| Tashkil::DammahTanwin),
            map(tag("َ"), |_| Tashkil::Fathah),
            map(tag("ِ"), |_| Tashkil::Kasrah),
            map(tag("ُ"), |_| Tashkil::Dammah),
        ))(input)
    }
}

/// Whether a UTF-8 character corresponds to an Arabic diacritic
pub fn is_diacritic(c: char) -> bool {
    matches!(c, 
        '\u{0610}'..='\u{0614}' // Honorifics
        | '\u{0615}'..='\u{061A}' // Quranic annotation signs
        | '\u{064B}'..='\u{065F}' // Tashkil from ISO 8859-6 + combining maddah and hamza + other combining marks
        | '\u{0670}' // Taskhil
        // Quranic annotation signs
        | '\u{06D6}'..='\u{06DC}'
        | '\u{06DF}'..='\u{06E8}' 
        | '\u{06EA}'..='\u{06ED}')
}

/// A fathah tashkil
pub const FATHAH: char = 'َ';

/// A kasrah tashkil
pub const KASRAH: char = 'ِ';

/// A dammah tashkil
pub const DAMMAH: char = 'ُ';

/// A shaddah tashkil
pub const SHADDAH: char = 'ّ';

/// A sukun tashkil
pub const SUKUN: char = 'ْ';

/// The maximum byte length of a conjugated verb before allocation is necessary
pub const BIG_VERB_LENGTH: usize = 24;

use std::ops::Index;

use nom::{branch::alt, IResult};
use smallstr::SmallString;
use ArabicLetter::*;

impl ArabicLetter {
    /// The list of arabic letters in Hijā'ī order
    pub const HIJAI: [ArabicLetter; 29] = [
        ا, ب, ت, ث, ج, ح, خ, د, ذ, ر, ز, س, ش, ص, ض, ط, ظ, ع, غ, ف, ق, ك, ل, م, ن, ه, و, ي, ء,
    ];

    /// Convert an Arabic letter to it's corresponding character
    #[inline]
    pub fn to_char(self) -> char {
        use ArabicLetter::*;
        match self {
            ا => 'ا',
            ب => 'ب',
            ت => 'ت',
            ث => 'ث',
            ج => 'ج',
            ح => 'ح',
            خ => 'خ',
            د => 'د',
            ذ => 'ذ',
            ر => 'ر',
            ز => 'ز',
            س => 'س',
            ش => 'ش',
            ص => 'ص',
            ض => 'ض',
            ط => 'ط',
            ظ => 'ظ',
            ع => 'ع',
            غ => 'غ',
            ف => 'ف',
            ق => 'ق',
            ك => 'ك',
            ل => 'ل',
            م => 'م',
            ن => 'ن',
            ه => 'ه',
            و => 'و',
            ي => 'ي',
            ء => 'ء',
        }
    }

    /// Get a character as an Arabic letter
    ///
    /// Returns `None` if the character is not in the Arabic alphabet
    ///
    /// - Alef hamzah (ٵ, إ), alef maddah (أَ), and alef maqsurah (ى) map to alef (أَ)
    /// - Waw hamzah (ؤ) maps to waw (و)
    /// - Ya hamzah (ئ,ࢨ) maps to ya (ي)
    /// - Ta marbutah (ة) maps to ta (ت)
    #[inline]
    pub fn from_char(c: char) -> Option<ArabicLetter> {
        [
            // ISO 8859-6
            Some(ء),
            Some(ا),
            Some(ا),
            Some(و),
            Some(ا),
            Some(ي),
            Some(ا),
            Some(ب),
            Some(ت),
            Some(ت),
            Some(ث),
            Some(ج),
            Some(ح),
            Some(خ),
            Some(د),
            Some(ذ),
            Some(ر),
            Some(ز),
            Some(س),
            Some(ش),
            Some(ص),
            Some(ض),
            Some(ط),
            Some(ظ),
            Some(ع),
            Some(غ),
            // Additions for early Persian and Azerbaijani
            None,
            None,
            None,
            None,
            None,
            // ISO 8859-6
            None,
            Some(ف),
            Some(ق),
            Some(ك),
            Some(ل),
            Some(م),
            Some(ن),
            Some(ه),
            Some(و),
            Some(ا),
            Some(ي),
            // Tashkil from ISO 8859-6
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            // Combining maddah and hamza
            None,
            None,
            None,
            // Other combining marks
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            // Arabic-Indic digits
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            // Punctuation
            None,
            None,
            None,
            None,
            // Archaic letters
            //TODO: think whether this is a good idea
            Some(ب),
            Some(ق),
            // Tashkil
            None,
            // Extended Arabic letters
            Some(ا),
            Some(ا),
            // Deprecated letter
            Some(ا),
        ]
        .get((c as usize).checked_sub(0x0621)?)
        .copied()
        .flatten()
    }

    /// Get this arabic letter as a defective consonant (W (و) or Y (ي)), if it is one
    #[inline]
    pub fn defective(self) -> Option<DefectiveConsonant> {
        match self {
            و => Some(DefectiveConsonant::W),
            ي => Some(DefectiveConsonant::Y),
            _ => None,
        }
    }
}

/// Whether a character is an Arabic letter
#[inline]
pub fn is_arabic_letter(c: char) -> bool {
    ArabicLetter::from_char(c).is_some()
}

/// A root for an Arabic Word
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Root<const N: usize = 3>(pub [ArabicLetter; N]);

impl Root {
    /// Whether this root is sound, i.e. not weak
    ///
    /// A root is sound if it is not assimilated, hollow, defective, doubled, or hamzated
    //TODO: are all hamzated but not first-hamzated words sound?
    #[inline]
    pub fn sound(&self) -> bool {
        self.assimilated().is_none()
            && self.hollow().is_none()
            && self.defective().is_none()
            && !self.doubled()
            && !self.first_hamzated()
    }

    /// Whether this root is assimilated (first-weak), of the W or Y type
    ///
    /// A root is assimilated (first-weak) if the first letter is  W (و) or Y (ي); in this case, this function returns `Some`
    /// Otherwise, `None` is returned.
    #[inline]
    pub fn assimilated(&self) -> Option<DefectiveConsonant> {
        self[0].defective()
    }

    /// Whether this root is hollow (second-weak), of the W or Y type
    ///
    /// A root is hollow (second-weak) if the second letter is  W (و) or Y (ي); in this case, this function returns `Some`
    /// Otherwise, `None` is returned.
    #[inline]
    pub fn hollow(&self) -> Option<DefectiveConsonant> {
        self[1].defective()
    }

    /// Whether this root is defective (third-weak), of the W or Y type
    ///
    /// A root is defective (third-weak) if the third letter is  W (و) or Y (ي); in this case, this function returns `Some`
    /// Otherwise, `None` is returned.
    #[inline]
    pub fn defective(&self) -> Option<DefectiveConsonant> {
        self[2].defective()
    }

    /// Whether this doubled (geminated)
    ///
    /// A root is doubled (geminated) if the second and third letters of the root are the same
    #[inline]
    pub fn doubled(&self) -> bool {
        self[2] == self[3]
    }

    /// Whether this root is hamzated
    ///
    /// A root is hamzated if it contains a hamzah (ء)
    #[inline]
    pub fn hamzated(&self) -> bool {
        self.0.contains(&ء)
    }

    /// Whether this root is first-hamzated, i.e. has a hamzah (ء) as it's first character
    #[inline]
    pub fn first_hamzated(&self) -> bool {
        self[0] == ء
    }
}

/// A quadriliteral root
pub type QuadRoot = Root<4>;

impl<const N: usize> Index<usize> for Root<N> {
    type Output = ArabicLetter;

    #[inline]
    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

/// Tenses for an Arabic verb
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Tense {
    /// Past tense (اَلماضي)
    Past,
    /// Present tense (المضارع)
    Present,
    /// Future tense (المستقبل)
    Future,
}

/// Form for an Arabic verb
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum VerbForm {
    /// Verb forms
    Verb(Voice, Number, Person, Gender),
    /// A participle
    Participle(Voice),
    /// A verbal noun (مصدر)
    VerbalNoun,
}

/// Voices for an Arabic verb
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Voice {
    /// Active voice
    Active,
    /// Passive voice
    Passive,
}

/// Grammatical genders for an Arabic verb
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Gender {
    /// Masculine grammatical gender
    Masculine,
    /// Feminine grammatical gender
    Feminine,
}

/// Persons for an Arabic verb
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Person {
    /// First person
    First,
    /// Second person
    Second,
    /// Third person
    Third,
}

/// Numbers for an Arabic verb
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Number {
    /// Singular
    Singular,
    /// Dual
    Dual,
    /// Plural
    Plural,
}

/// Moods for an Arabic verb
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Moods {
    /// Indicative
    Indicative,
    /// Subjunctive
    Subjunctive,
    /// Jussive
    Jussive,
    /// Imperative
    Imperative,
    /// Short energetic
    ShortEnergetic,
    /// Long energetic
    LongEnergetic,
}

/// Forms for an Arabic verb based on a triliteral root
///
/// Examples are given using the root ك-ت-ب (write), except for Form IX, which is demonstrated using ح-م-ر (red)
/// (Obtained from [Wikipedia](https://en.wikipedia.org/wiki/Arabic_verbs#Derivational_categories,_conjugations))
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Form {
    /// Form I
    ///
    /// e.g. kataba كَتَبَ (past: he wrote) or yaktubu يَكْتُبُ (non-past: he writes)
    FI,
    /// Form II
    ///
    /// e.g. kattaba كَتَّبَ (past: he made (someone) write) or yukattibu يُكَتِّبُ (non-past: he makes (someone) write)
    FII,
    /// Form III
    ///
    /// e.g. kātaba كاتَبَ (past: he wrote to (someone)) or yukātibu يُكاتِبُ (non-past: he writes to (someone))
    FIII,
    /// Form IV
    ///
    /// e.g. 'aktaba أكْتَبَ (past: he dictated) or yuktibu يُكْتِبُ (non-past: he dictates)
    FIV,
    /// Form V
    ///
    /// e.g. takattaba تَكَتَّبَ (past: invalid) or yatakattabu يَتَكَتُّبُ (non-past: invalid)
    FV,
    /// Form VI
    ///
    /// e.g. takātaba تَكَاتَبَ (past: he corresponded) or yatakātabu يَتَكَاتَبَ
    // TODO: should this be yatakātaba or يَتَكَاتَبُ?
    FVI,
    /// Form VII
    ///
    /// e.g. inkataba اِنْكَتَبَ (past: he subscribed) or yankatibu يَنْكَتِبُ (non-past: he subscribes)
    FVII,
    /// Form VIII
    ///
    /// e.g. iktataba اِكْتَتَبَ (past: he copied) or yaktatibu يَكْتَتِبُ (non-past: he copies)
    FVIII,
    /// Form IX
    ///
    /// e.g. iḥmarra اِحْمَرَّ (past: he turned red) or yaḥmarru يحْمَرُّ (non-past: he turns red)
    FIX,
    /// Form X
    ///
    /// e.g. istaktaba اِسْتَكْتَبَ (past: he asked (someone) to write) or yastaktibu يَسْتَكْتِبُ (non-past: he asks (someone) to write)
    FX,
}

impl Form {
    /// A list of common forms
    pub const COMMON: [Form; 10] = [
        Form::FI,
        Form::FII,
        Form::FIII,
        Form::FIV,
        Form::FV,
        Form::FVI,
        Form::FVII,
        Form::FVIII,
        Form::FIX,
        Form::FX,
    ];

    /// Conjugate a root into the past form (الماضي), without applying any exceptions
    pub fn simple_past(self, root: Root, tashkil: bool) -> SmallString<[u8; BIG_VERB_LENGTH]> {
        let mut result = SmallString::new();
        match self {
            Form::FI => {
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
            Form::FII => {
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(SHADDAH);
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
            Form::FIII => {
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
            Form::FIV => {
                //if tashkil {
                result.push('أ');
                //} else {
                //    result.push('ا');
                //}
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
            Form::FV => {
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                result.push(root[1].to_char());
                if tashkil {
                    result.push(SHADDAH);
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
            Form::FVI => {
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
            Form::FVII => {
                result.push('ا');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push('ن');
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
            Form::FVIII => {
                result.push('ا');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
            Form::FIX => {
                result.push('ا');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(SHADDAH);
                    result.push(FATHAH);
                }
            }
            Form::FX => {
                result.push('ا');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push('س');
                if tashkil {
                    result.push(SUKUN);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
        }
        result
    }

    /// Conjugate a root into the present form (المضارع), without applying any exceptions
    pub fn simple_present(self, root: Root, tashkil: bool) -> SmallString<[u8; BIG_VERB_LENGTH]> {
        let mut result = SmallString::new();
        match self {
            Form::FI => {
                result.push('ي');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SHADDAH)
                }
                result.push(root[1].to_char());
                result.push(root[2].to_char());
                if tashkil {
                    result.push(DAMMAH)
                }
            }
            Form::FII => {
                result.push('ي');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                    result.push(SHADDAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(DAMMAH);
                }
            }
            Form::FIII => {
                result.push('ي');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(DAMMAH);
                }
            }
            Form::FIV => {
                result.push('ي');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(DAMMAH);
                }
            }
            Form::FV => {
                result.push('ي');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(SHADDAH);
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(DAMMAH);
                }
            }
            Form::FVI => {
                result.push('ي');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(DAMMAH);
                }
            }
            Form::FVII => {
                result.push('ي');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push('ن');
                if tashkil {
                    result.push(SUKUN)
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(DAMMAH);
                }
            }
            Form::FVIII => {
                result.push('ي');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN)
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(DAMMAH);
                }
            }
            Form::FIX => {
                result.push('ي');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN)
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(SHADDAH);
                    result.push(DAMMAH);
                }
            }
            Form::FX => {
                result.push('ي');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push('س');
                if tashkil {
                    result.push(SUKUN);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN)
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(DAMMAH);
                }
            }
        }
        result
    }

    /// Conjugate a root into the active participle (اسم المفعول), without applying any exceptions
    pub fn active_participle(
        self,
        root: Root,
        tashkil: bool,
    ) -> SmallString<[u8; BIG_VERB_LENGTH]> {
        let mut result = SmallString::new();
        match self {
            Form::FI => {
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH)
                }
                result.push(root[2].to_char());
            }
            Form::FII => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                    result.push(SHADDAH);
                }
                result.push(root[2].to_char())
            }
            Form::FIII => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH)
                }
                result.push(root[2].to_char())
            }
            Form::FIV => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH)
                }
                result.push(root[2].to_char())
            }
            Form::FV => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                    result.push(SHADDAH)
                }
                result.push(root[2].to_char())
            }
            Form::FVI => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char())
            }
            Form::FVII => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push('ن');
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char())
            }
            Form::FVIII => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char())
            }
            Form::FIX => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
                if tashkil {
                    result.push(SHADDAH);
                }
            }
            Form::FX => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push('س');
                if tashkil {
                    result.push(SUKUN);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char());
            }
        }
        result
    }

    /// Conjugate a root into the passive participle (اسم الفاعل), without applying any exceptions
    pub fn passive_participle(
        self,
        root: Root,
        tashkil: bool,
    ) -> Result<SmallString<[u8; BIG_VERB_LENGTH]>, ()> {
        let mut result = SmallString::new();
        match self {
            Form::FI => {
                result.push('م');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN)
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(DAMMAH)
                }
                result.push('و');
                result.push(root[2].to_char());
            }
            Form::FII => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(SHADDAH);
                    result.push(FATHAH);
                }
                result.push(root[2].to_char())
            }
            Form::FIII => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH)
                }
                result.push(root[2].to_char())
            }
            Form::FIV => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH)
                }
                result.push(root[2].to_char());
            }
            Form::FV => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(SHADDAH);
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
            Form::FVI => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH)
                }
                result.push(root[2].to_char());
            }
            Form::FVII => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push('ن');
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH)
                }
                result.push(root[2].to_char());
            }
            Form::FVIII => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH)
                }
                result.push(root[2].to_char());
            }
            Form::FIX => return Err(()),
            Form::FX => {
                result.push('م');
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push('س');
                if tashkil {
                    result.push(SUKUN);
                }
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[2].to_char());
            }
        }
        Ok(result)
    }

    /// Conjugate a root into a verbal noun (المصدر), without applying any exceptions
    pub fn verbal_noun(
        self,
        root: Root,
        tashkil: bool,
    ) -> Result<SmallString<[u8; BIG_VERB_LENGTH]>, ()> {
        let mut result = SmallString::new();
        match self {
            Form::FI => return Err(()),
            Form::FII => {
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN)
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH)
                }
                result.push('ي');
                result.push(root[2].to_char())
            }
            Form::FIII => {
                //TODO: consider مُفاعَلَة
                result.push(root[0].to_char());
                if tashkil {
                    result.push(KASRAH)
                }
                result.push(root[1].to_char());
                result.push('ا');
                result.push(root[2].to_char());
            }
            Form::FIV => {
                result.push('إ');
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN)
                }
                result.push(root[1].to_char());
                result.push('ا');
                result.push(root[2].to_char());
            }
            Form::FV => {
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(SHADDAH);
                    result.push(DAMMAH);
                }
                result.push(root[2].to_char());
            }
            Form::FVI => {
                result.push('ت');
                if tashkil {
                    result.push(FATHAH);
                }
                result.push(root[0].to_char());
                result.push('ا');
                result.push(root[1].to_char());
                if tashkil {
                    result.push(DAMMAH);
                }
                result.push(root[2].to_char());
            }
            Form::FVII => {
                result.push('ا');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push('ن');
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[1].to_char());
                result.push('ا');
                result.push(root[2].to_char());
            }
            Form::FVIII => {
                result.push('ا');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push('ت');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[1].to_char());
                result.push('ا');
                result.push(root[2].to_char());
            }
            Form::FIX => {
                //TODO: fixme
                result.push('ا');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[2].to_char());
                result.push('ا');
                result.push('ل');
            }
            Form::FX => {
                result.push('ا');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push('س');
                if tashkil {
                    result.push(SUKUN);
                }
                result.push('ت');
                if tashkil {
                    result.push(KASRAH);
                }
                result.push(root[0].to_char());
                if tashkil {
                    result.push(SUKUN);
                }
                result.push(root[1].to_char());
                result.push('ا');
                result.push(root[2].to_char());
            }
        }
        Ok(result)
    }
}

/// Forms for an Arabic verb based on a quadriliteral root
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum QuadForm {
    /// Form Iq
    QI,
    /// Form IIq
    QII,
    /// Form IIIq
    QIII,
    /// Form IVq
    QIV,
}

/// Defective root consonants
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum DefectiveConsonant {
    /// w, or و
    W,
    /// y, or ي
    Y,
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn ktb_sanity_test() {
        let ktb = Root([ك, ت, ب]);
        let forms = [
            "كَتَب",
            "كَتَّب",
            "كاتَب",
            "أكْتَب",
            "تَكتَّب",
            "تَكاتَب",
            "اِنْكَتَب",
            "اِكْتَتَب",
            "اِكْتَبَّ", // Not actually valid for this type of word!
            "اِسْتَكْتَب",
        ];
        for (i, form) in Form::COMMON.iter().enumerate() {
            assert!(forms[i].len() <= BIG_VERB_LENGTH);
            assert_eq!(form.simple_past(ktb, true), forms[i]);
            assert_eq!(form.simple_past(ktb, false), tashkil::remove(forms[i]));
        }
    }

    #[test]
    fn unicode_0x0600_0x06ff_roundtrip() {
        for c in '\u{0600}'..='\u{06ff}' {
            if let Some(v) = ArabicLetter::from_char(c) {
                let cv = v.to_char();
                let rv = ArabicLetter::from_char(cv);
                assert_eq!(
                    Some(v),
                    rv,
                    "Failed to roundtrip {c:?} (u{:04x}) --> {v:?} --> {cv:?} (u{:04x}) --> {rv:?}",
                    c as usize,
                    cv as usize,
                )
            }
        }
    }

    #[test]
    fn abjad_roundtrip() {
        for letter in ArabicLetter::HIJAI {
            assert_eq!(ArabicLetter::from_char(letter.to_char()), Some(letter))
        }
    }

    #[test]
    fn tashkil_are_diacritics() {
        assert!(is_diacritic(FATHAH));
        assert!(is_diacritic(KASRAH));
        assert!(is_diacritic(DAMMAH));
        assert!(is_diacritic(SHADDAH));
        assert!(is_diacritic(SUKUN));
    }
}
